<div class="section_head">
  <h2 class="section_head_title"><i>高額査定のチャンス！</i><br>強化買取ブランド一覧</h2>
</div>
<div class="kaitori_genre">
  <ul class="kaitori_genre_list">
    <li class="kaitori_genre_list_item genre--outdoor">
      <h3 class="kaitori_brand_title">アウトドア</h3>
      <ul class="kaitori_brand_list">
        <li class="kaitori_brand_list_item"><a href="#">ARC'TERYX</a></li>
        <li class="kaitori_brand_list_item"><a href="#">THE NORTH FACE</a></li>
        <li class="kaitori_brand_list_item"><a href="#">White Mountaineering</a></li>
        <li class="kaitori_brand_list_item"><a href="#">MONCLER</a></li>
      </ul>
    </li>
    <li class="kaitori_genre_list_item genre--bag">
      <h3 class="kaitori_brand_title">バッグ</h3>
      <ul class="kaitori_brand_list">
        <li class="kaitori_brand_list_item"><a href="#">PORTER</a></li>
        <li class="kaitori_brand_list_item"><a href="#">BRIEFING</a></li>
        <li class="kaitori_brand_list_item"><a href="#">RIMOWA</a></li>
        <li class="kaitori_brand_list_item"><a href="#">TUMI</a></li>
      </ul>
    </li>
    <li class="kaitori_genre_list_item genre--shoes">
      <h3 class="kaitori_brand_title">靴</h3>
      <ul class="kaitori_brand_list">
        <li class="kaitori_brand_list_item"><a href="#">RED WING</a></li>
        <li class="kaitori_brand_list_item"><a href="#">WHITE'S</a></li>
        <li class="kaitori_brand_list_item"><a href="#">Paraboot</a></li>
        <li class="kaitori_brand_list_item"><a href="#">Tricker's</a></li>
      </ul>
    </li>
    <li class="kaitori_genre_list_item genre--watch">
      <h3 class="kaitori_brand_title">時計</h3>
      <ul class="kaitori_brand_list">
        <li class="kaitori_brand_list_item"><a href="#">G-SHOCK</a></li>
        <li class="kaitori_brand_list_item"><a href="#">TIMEX</a></li>
        <li class="kaitori_brand_list_item"><a href="#">SUUNTO</a></li>
        <li class="kaitori_brand_list_item"><a href="#">NIXON</a></li>
      </ul>
    </li>
    <li class="kaitori_genre_list_item genre--glasses">
      <h3 class="kaitori_brand_title">メガネ</h3>
      <ul class="kaitori_brand_list">
        <li class="kaitori_brand_list_item"><a href="#">泰八郎謹製</a></li>
        <li class="kaitori_brand_list_item"><a href="#">白山眼鏡店</a></li>
        <li class="kaitori_brand_list_item"><a href="#">小竹長兵衛作</a></li>
        <li class="kaitori_brand_list_item"><a href="#">Ray-Ban</a></li>
      </ul>
    </li>
    <li class="kaitori_genre_list_item genre--clothing">
      <h3 class="kaitori_brand_title">アメカジ衣類</h3>
      <ul class="kaitori_brand_list">
        <li class="kaitori_brand_list_item"><a href="#">Engineered Garments</a></li>
        <li class="kaitori_brand_list_item"><a href="#">W）taps</a></li>
        <li class="kaitori_brand_list_item"><a href="#">BLUE BLUE</a></li>
        <li class="kaitori_brand_list_item"><a href="#">東洋エンタープライズ</a></li>
      </ul>
    </li>
  </ul>
</div><!-- [end] .kaitori_genre -->