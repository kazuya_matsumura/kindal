<!-- [start] include footer.php -->  
<footer class="footContent">
  <address class="footContent_contact">
		<div class="footContent_contact_detail">
			<h2 class="footContent_contact_detail_title"><img src="./assets/images/siteid.png" alt="Kindal 東心斎橋店"></h2>
			<p class="footContent_contact_detail_lead">アメカジ＆アウトドア　ブランド古着 買取販売</p>
			<p class="footContent_contact_detail_address">〒542-0083<br>大阪市中央区東心斎橋1-16-15オクノビル2F<br>大阪/地下鉄心斎橋駅徒歩6分
			</p>
			<p class="footContent_contact_detail_tel">
        <span class="for-PC">06-6281-4567</span>
        <a href="tel:06-6281-4567" class="for-SP">06-6281-4567</a>
      </p>
			<p class="footContent_contact_detail_open">
				<span class="footContent_contact_detail_open_time">OPEN 11:00~23:00</span>&emsp;<span class="footContent_contact_detail_open_holiday">水曜定休</span>
			</p>
		</div><!-- [end] .footContent_contact_detail -->

		<div class="footContent_contact_map">
			<iframe class="footContent_contact_map_body" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13125.248978691623!2d135.5026029!3d34.6720681!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa30ac09e89ee1c90!2z44Kr44Kk44Oz44OJ44Kq44Or5p2x5b-D5paO5qmL5bqX!5e0!3m2!1sja!2sjp!4v1567256271965!5m2!1sja!2sjp" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>	<!-- [end] .footContent_contact_map -->	
  </address><!-- [end] .footContent_contact -->

	<nav class="footContent_navi">
		<div class="footContent_navi_kaitori">
			<h2 class="footContent_navi_kaitori_title"><a href="">買取方法</a></h2>
			<a href="kaitori_shop.php" class="footContent_navi_item">店頭買取</a>
			<a href="kaitori_delivery.php" class="footContent_navi_item">宅配買取</a>
			<a href="#" class="footContent_navi_item">宅配キットお申込み</a>
			<a href="kaitori_line.php" class="footContent_navi_item">LINE査定</a>
		</div>
		<div class="footContent_navi_list footContent_navi_list--01">
			<a href="result.php" class="footContent_navi_item">高価買取実績</a>
			<a href="news.php" class="footContent_navi_item">ニュース</a>
			<a href="#" class="footContent_navi_item">コラム</a>
			<a href="#" class="footContent_navi_item">オーナーブログ</a>
			<a href="faq.php" class="footContent_navi_item">よくあるご質問</a>
		</div>
		<div class="footContent_navi_list footContent_navi_list--02">
			<a href="target.php" class="footContent_navi_item">販売について</a>
			<a href="#" target="_blank" class="footContent_navi_item">Youtube</a> 
			<a href="https://www.instagram.com/kindal_higashishinsaibashi/" target="_blank" class="footContent_navi_item">Instagram</a>
			<a href="#" target="_blank" class="footContent_navi_item">Facebook</a>
		</div>
	</nav><!-- [end] .footContent_navi -->	
  <div class="footContent_copyright">
    <small class="footContent_copyright_text">&copy; 2019 カインドオル東心斎橋店</small>
  </div><!-- [end] .footContent_copyright -->
</footer>
  
</div><!-- [end] .wrapper -->
  
<!-- javascript -->
<script type="text/javascript" src="./assets/libraries/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="./assets/js/common.js"></script>
</body>
</html>
<!-- [end] include footer.php -->