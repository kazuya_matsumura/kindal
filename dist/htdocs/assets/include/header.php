<!-- [start] include header.php -->
<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="<?php echo PAGE_DESC; ?>">
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="<?php echo PAGE_TITLE; ?>">
<meta name="twitter:description" content="<?php echo PAGE_DESC; ?>">
<meta name="twitter:image" content="<?php echo (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST']; ?>/assets/images/og-image.png">
<meta name="theme-color" content="#fff">
<meta name="apple-mobile-web-app-title" content="大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
<meta property="og:title" content="<?php echo PAGE_TITLE; ?>">
<meta property="og:description" content="<?php echo PAGE_DESC; ?>">
<meta property="og:site_name" content="大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞">
<meta property="og:image" content="<?php echo (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST']; ?>/assets/images/og-image.png">
<link rel="canonical" href="<?php echo (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
<title><?php echo PAGE_TITLE; ?></title>
 
<!-- font -->
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:500,700&display=swap&subset=japanese" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:500&display=swap" rel="stylesheet">
  
<!-- css -->
<link rel="stylesheet" type="text/css" href="./assets/css/reset.css">
<link rel="stylesheet" type="text/css" href="./assets/css/global.css">
<link rel="stylesheet" type="text/css" href="./assets/css/global_sp.css" media="screen and (max-width:750px)">
</head>

<body id="body">
<div class="wrapper <?php echo PAGE_CAT; ?>">

<header class="headContent">	
	<div class="headContent_inner">
		<div class="headContent_siteid">
			<a class="headContent_siteid_logo" href="index.php"><img src="./assets/images/siteid.png" alt="Kindal 東心斎橋店"></a>			
		</div>
		<p class="headContent_siteid_lead"><span>アメカジ＆アウトドア<br>ブランド古着 買取販売</span></p>
		<div class="headContent_contact">
			<p class="headContent_contact_open">
				<span class="headContent_contact_open_time">OPEN 11:00~23:00</span>&emsp;<span class="headContent_contact_open_holiday">水曜定休</span>
			</p>
			<div class="headContent_tel">
				<span class="headContent_tel_body for-PC">06-6281-4567</span>
				<a href="tel:06-6281-4567" class="headContent_tel_body for-SP">06-6281-4567</a>
			</div>
		</div>			
	</div>
  
  <nav class="headContent_navi--PC">
    <ul class="headContent_navi_list">
      <li class="headContent_navi_list_item"><a href="kaitori_shop.php">買取方法</a></li>
      <li class="headContent_navi_list_item"><a href="result.php">高価買取実績</a></li>
      <li class="headContent_navi_list_item"><a href="shopping.php">ショッピング</a></li>
      <li class="headContent_navi_list_item"><a href="news.php">ニュース</a></li>
      <li class="headContent_navi_list_item"><a href="#">コラム</a></li>
      <li class="headContent_navi_list_item"><a href="#">オーナーブログ</a></li>
      <li class="headContent_navi_list_item"><a href="faq.php">よくあるご質問</a></li>
    </ul>      
  </nav>

  <a href="javascript:void(0)" class="navi_toggle">
    <span class="navi_toggle_bar bar01"></span>
    <span class="navi_toggle_bar bar02"></span>
    <span class="navi_toggle_bar bar03"></span>
  </a>
  <nav class="headContent_navi--SP">
    <ul class="headContent_navi_list">
      <li class="headContent_navi_list_item"><a href="kaitori_shop.php">店頭買取</a></li>
      <li class="headContent_navi_list_item"><a href="kaitori_delivery.php">宅配買取</a></li>
      <li class="headContent_navi_list_item"><a href="#">宅配キットお申込み</a></li>
      <li class="headContent_navi_list_item"><a href="kaitori_line.php">LINE査定</a></li>
      <li class="headContent_navi_list_item"><a href="result.php">高価買取実績</a></li>
      <li class="headContent_navi_list_item"><a href="shopping.php">ショッピング</a></li>
      <li class="headContent_navi_list_item"><a href="news.php">ニュース</a></li>
      <li class="headContent_navi_list_item"><a href="#">コラム</a></li>
      <li class="headContent_navi_list_item"><a href="#">オーナーブログ</a></li>
      <li class="headContent_navi_list_item"><a href="faq.php">よくあるご質問</a></li>
      <li class="headContent_navi_list_item"><a href="target.php">販売について</a></li>
      <li class="headContent_navi_list_item"><a href="https://www.youtube.com/channel/UC9COae_RSw4ah_c7I4sCU8A" target="_blank">Youtube</a></li>
      <li class="headContent_navi_list_item"><a href="https://www.instagram.com/kindal_higashishinsaibashi/" target="_blank">Instagram</a></li>
      <li class="headContent_navi_list_item"><a href="https://www.facebook.com/kind.higashishinsaibashi/" target="_blank">Facebook</a></li>
    </ul>
	</nav><!-- [end] .footContent_navi -->

</header>
<!-- [end] include header.php -->