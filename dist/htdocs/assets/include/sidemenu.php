<!-- [start] include sidemenu.php -->  
<div class="sideMenu">
  <div class="sideMenu_search">
    <form class="sideMenu_search_form" action="/search" method="get" name="frmsrh">
      <fieldset>
        <legend class="sideMenu_search_form_name">検索</legend>
        <div class="sideMenu_search_form_input">
          <div class="sideMenu_search_form_input_inner">
            <input class="sideMenu_search_form_input_text" value="" placeholder="キーワード検索" type="search" autocomplete="off" name="k" required="">
          </div>
        </div>
        <span class="sideMenu_search_form_button" onclick="document.frmsrh.submit();">
          <input type="submit" class="" title="検索">
        </span>
      </fieldset>
    </form>
  </div>

  <div class="sideMenu_section sideMenu_section--brand">
    <div class="sideMenu_head">
      <h3 class="sideMenu_head_title">買取ブランド一覧</h3>
    </div>
    <div class="kaitori_genre">
      <ul class="kaitori_genre_list">
        <li class="kaitori_genre_list_item genre--outdoor">
          <h3 class="kaitori_brand_title">アウトドア</h3>
          <ul class="kaitori_brand_list">
            <li class="kaitori_brand_list_item"><a href="#">ARC'TERYX（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">THE NORTH FACE（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">White Mountaineering（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">MONCLER（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">CANADA GOOSE（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">MAMMUT（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">patagonia（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">Rocky Mountain Featherbed（15）</a></li>
          </ul>
        </li>
        <li class="kaitori_genre_list_item genre--bag">
          <h3 class="kaitori_brand_title">バッグ</h3>
          <ul class="kaitori_brand_list">
            <li class="kaitori_brand_list_item"><a href="#">PORTER（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">BRIEFING（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">RIMOWA（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">TUMI（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">ZERO HALLIBURTON（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">Gregory（15）</a></li>
          </ul>
        </li>
        <li class="kaitori_genre_list_item genre--shoes">
          <h3 class="kaitori_brand_title">靴</h3>
          <ul class="kaitori_brand_list">
            <li class="kaitori_brand_list_item"><a href="#">RED WING（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">WHITE'S（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">Paraboot（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">Tricker's（15）</a></li>
          </ul>
        </li>
        <li class="kaitori_genre_list_item genre--watch">
          <h3 class="kaitori_brand_title">時計</h3>
          <ul class="kaitori_brand_list">
            <li class="kaitori_brand_list_item"><a href="#">G-SHOCK（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">TIMEX（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">SUUNTO（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">NIXON（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">Luminox（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">TAG Heuer（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">BREITLING（15）</a></li>
          </ul>
        </li>
        <li class="kaitori_genre_list_item genre--glasses">
          <h3 class="kaitori_brand_title">メガネ</h3>
          <ul class="kaitori_brand_list">
            <li class="kaitori_brand_list_item"><a href="#">泰八郎謹製（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">白山眼鏡店（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">小竹長兵衛作（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">Ray-Ban（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">OAKLEY（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">EFFECTOR（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">999.9（15）</a></li>
          </ul>
        </li>
        <li class="kaitori_genre_list_item genre--clothing">
          <h3 class="kaitori_brand_title">アメカジ衣類</h3>
          <ul class="kaitori_brand_list">
            <li class="kaitori_brand_list_item"><a href="#">Engineered Garments（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">W)taps（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">BLUE BLUE（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">東洋エンタープライズ（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">WAREHOUSE（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">BRU NA BOINNE（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">FILSON（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">KATO`（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">THE REAL McCOY'S（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">SKOOKUM（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">REMI RELIEF（15）</a></li>
            <li class="kaitori_brand_list_item"><a href="#">RRL（15）</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- [end] .kaitori_genre--full -->
  </div>        
  <div class="sideMenu_section sideMenu_section--archive">
    <div class="sideMenu_head">
      <h3 class="sideMenu_head_title">アーカイブ</h3>
    </div>
    <ul class="sideMenu_list">
      <li class="sideMenu_list_item"><a href="#">2019年8月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年7月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年6月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年5月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年4月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年3月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年2月（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">2019年1月（15）</a></li>
    </ul>
  </div>
  <div class="sideMenu_section sideMenu_section--enhance">
    <div class="sideMenu_head">
      <h3 class="sideMenu_head_title">強化買取ブランド</h3>
    </div>
    <ul class="sideMenu_list">
      <li class="sideMenu_list_item"><a href="#">G-SHOCK（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">TIMEX（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">SUUNTO（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">NIXON（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">Luminox（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">TAG Heuer（15）</a></li>
      <li class="sideMenu_list_item"><a href="#">BREITLING（15）</a></li>
    </ul>
  </div><div class="sideMenu_section sideMenu_section--post">
    <div class="sideMenu_head">
      <h3 class="sideMenu_head_title">POST</h3>
    </div>
    <ul class="sideMenu_list">
      <li class="sideMenu_list_item"><a href="#">ニュース</a></li>
      <li class="sideMenu_list_item"><a href="#">コラム</a></li>
      <li class="sideMenu_list_item"><a href="#">オーナーブログ</a></li>
      <li class="sideMenu_list_item"><a href="#">買取対象商品</a></li>
      <li class="sideMenu_list_item"><a href="#">高価買取実績</a></li>
      <li class="sideMenu_list_item"><a href="#">オリジナル商品</a></li>
    </ul>
  </div>
</div>
<!-- [end] .sideMenu -->