// JavaScript Document

/* グローバルナビメニュー オープンボタン（SP）*/
const menuOpenBtnSP = document.getElementsByClassName('navi_toggle')[0];
const menuBody = document.getElementsByClassName('headContent_navi--SP')[0];
menuOpenBtnSP.addEventListener('click',function(){
	console.log('click');
  menuOpenBtnSP.classList.toggle('is-opened');
  menuBody.classList.toggle('is-opened');
},false);


/* jQuery */
$(document).ready(function(){
	
/**
* タッチデバイスの判定・タップ制御 (jQuery)
*/
  function isTouchDevice() {
    let result = false;
    if (window.ontouchstart === null) {
      result = true;
    }
    return result;
  }

  if (isTouchDevice()) {
    $('.wrapper').addClass('is-touch-device').attr('ontouchstart', ' ');
    console.log('touch device');
  } else {
    $('.wrapper').removeClass('is-touch-device');
  }

  
/**
*スムーススクロール (jQuery)
*/
  let headerHight = $(".headContent").innerHeight(); //ヘッダの高さ
  //URLのハッシュ値を取得
  var urlHash = location.hash;
  //ハッシュ値があればページ内スクロール
  if(urlHash) {
    //スクロールを0に戻す
    $('body,html').stop().scrollTop(0);
    setTimeout(function () {
      //ロード時の処理を待ち、時間差でスクロール実行
      scrollToAnker(urlHash) ;
    }, 100);
  }

  //通常のクリック時
  $('a[href^="#"]').click(function() {
    //ページ内リンク先を取得
    var href= $(this).attr("href");
    //リンク先が#か空だったらhtmlに
    var hash = href == "#" || href == "" ? 'html' : href;
    //スクロール実行
    scrollToAnker(hash);
    //リンク無効化
    return false;
  });

  // 関数：スムーススクロール
  // 指定したアンカー(#ID)へアニメーションでスクロール
  function scrollToAnker(hash) {
    var target = $(hash);
    var position = target.offset().top - headerHight;
    //var position = target.offset().top;
    $('body,html').stop().animate({scrollTop:position}, 400);
  }
});