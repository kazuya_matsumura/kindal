<?php
define ('PAGE_CAT' , 'category');
define ('PAGE_ID' , 'news');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , 'ニュース｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
	
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">ニュース</h1>
    </div>
  </div>
  
  <div class="page_body">    
    <div class="section section--post">
      <ul class="post_list">
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb01.jpg" alt=""></div>
            <p class="post_list_item_text">セール｜いつでも10％OFFサービス</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb02.jpg" alt=""></div>
            <p class="post_list_item_text">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅲ – お客様の人生分岐点？</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb03.jpg" alt=""></div>
            <p class="post_list_item_text">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅱ – 大阪中央区で大阪を満喫</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb01.jpg" alt=""></div>
            <p class="post_list_item_text">セール｜いつでも10％OFFサービス</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb02.jpg" alt=""></div>
            <p class="post_list_item_text">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅲ – お客様の人生分岐点？</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb03.jpg" alt=""></div>
            <p class="post_list_item_text">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅱ – 大阪中央区で大阪を満喫</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb01.jpg" alt=""></div>
            <p class="post_list_item_text">セール｜いつでも10％OFFサービス</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb02.jpg" alt=""></div>
            <p class="post_list_item_text">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅲ – お客様の人生分岐点？</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb03.jpg" alt=""></div>
            <p class="post_list_item_text">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅱ – 大阪中央区で大阪を満喫</p>
          </a>
        </li>        
      </ul>
      <div class="pagenation">
        <ul class="pagenation_list">
          <li><a class="progress" href="#">1/3ページ</a></li>
          <li><span class="current">1</span></li>
          <li><a class="num" href="#">2</a></li>
          <li><a class="num" href="#">3</a></li>
        </ul>
      </div>      
    </div><!-- [end] .section -->
    
    <div class="section section--brand">
      <?php require_once (dirname(__FILE__) . '/assets/include/brand_list.php'); ?>
    </div>
    
    <div class="section">
      <div class="kaitori_banner">
        <?php require_once (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
      </div>
    </div><!-- [end] .section -->
    
  </div><!-- [end] .page_body -->
</div><!-- [end] .main--category -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>