<?php
define ('PAGE_CAT' , 'contact');
define ('PAGE_ID' , 'inquiry');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '');
?>
<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">お問い合わせ</h1>
    </div>
  </div>  
  <div class="page_body">    
    <div class="section section--intro">
      <p class="contact_intro_text">下記フォームに必要事項を入力後、送信ボタンを押してください。</p>
    </div>

    <div class="section section--form">
      <form id="" name="" method="" autocomplete="">
        <div class="form_body">
          <div class="form_item">
            <label for="" class="form_item_title">お名前<span class="required">必須</span></label>          
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />
          </div>
          <div class="form_item">
            <label for="" class="form_item_title">メールアドレス<span class="required">必須</span></label>
            <input type="email" name="" id="" class="form_item_input_text" placeholder="" value="" />          
          </div>
          <div class="form_item">
            <label for="" class="form_item_title">お電話番号<span class="required">必須</span></label>
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />         
          </div>          
          <div class="form_item">
            <label for="" class="form_item_title">ご希望の回答方法<span class="required">必須</span></label>
            <div class="form_item_input_wrap">
               <select name="returen-menu" class="form_item_input_select" aria-invalid="false">
                 <option value="">---</option>
                 <option value="メール">メール</option>
                 <option value="電話">電話</option>
              </select>
            </div>
          </div>
          <!--<div class="form_item">
            <label for="" class="form_item_title">郵便番号</label>
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />        
          </div>-->
          <div class="form_item">
            <label for="" class="form_item_title">ご住所</label>
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />
          </div>  
          <div class="form_item">
            <label for="" class="form_item_title">メッセージ本文</label>
            <div class="form_item_input_wrap">
              <textarea name="quote" cols="40" rows="10" class="form_item_input_textarea" aria-required="true" aria-invalid="false"></textarea>
            </div>            
          </div>

          <div class="button_wrap"><input type="submit" value="送信する" class="form_button submit" /></div>
    
        </div><!-- [end] .form_body -->

        <div class="wpcf7-response-output wpcf7-display-none"></div><!-- 動作メッセージ -->        
      </form>
      <p class="caption">※お問い合わせ受付後、お問い合わせ内容を記載した「お問い合わせ受付完了メール」を自動的に送信しております。<br>
お問い合わせ受付完了メールが届かない場合は正しくお問い合わせが送信されていない可能性がございます。</p>          
      <p class="caption">お手数をおかけいたしますが<br>
または<br>
お電話【　06-6281-4567　】<br>
にてお問い合わせください。</p>

    </div><!-- [end] .section_form -->
    

  </div><!-- [end] .page_body -->
</div><!-- [end] .main -->
  
<?php
include_once (dirname(__FILE__) . '/assets/include/footer.php');
?>