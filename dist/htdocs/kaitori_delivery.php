<?php
define ('PAGE_CAT' , 'subpage');
define ('PAGE_ID' , 'kaitori_delivery');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '宅配買取｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">宅配買取</h1>
    </div>
  </div>
	<div class="page_body">
		<div class="section section--intro">
			<p class="intro_lead">
				<span>送料無料</span>
				<span>査定無料</span>
				<span>キャンセル無料</span>
				<strong>3つの無料で安心♪是非お任せください！</strong>
			</p>					
			<p class="intro_text">使っていないブランド古着・バッグ・靴などございましたら是非、 無料宅配買取査定をご利用ください。<br>段ボールや伝票も当店にてご用意いたしますので、ご自宅で不要なアイテムを箱に入れるだけ！ <br>もちろん配送や買取ご成立後の代金振込手数料などは当店が負担いたしますので、お客様は「無料」でご利用いただけます！</p>
		</div>
		
		<div class="section section--question">
			<ul class="question_list">
				<li class="question_list_item"><div class="question_list_item_inner">あんまり有名じゃないけど、<br>このブランドは買取してくれる？</div></li>
				<li class="question_list_item"><div class="question_list_item_inner">箱捨てちゃったけど、高かった<br>ブランドシューズは買ってくれる？</div></li>
			</ul>
			<ul class="question_list">
				<li class="question_list_item"><div class="question_list_item_inner">あんまり有名じゃないけど、<br>このブランドは買取してくれる？</div></li>
				<li class="question_list_item"><div class="question_list_item_inner">箱捨てちゃったけど、高かった<br>ブランドシューズは買ってくれる？</div></li>
			</ul>
			<p class="question_answer">など、買い取りに関してご不明点がございましたら、<br>まずは、下記の方法でお気軽にお問い合わせください。</p>
			<p class="question_notice">※メンズ・レディース共に幅広いブランドをお取り扱い致しており、<br>できる限り全てをお買取りできるように心がけております。</p>
			<div class="question_contact">
				<a href="" class="question_contact_button question_contact_button--line"><div class="question_contact_button_inner">LINEでお問い合わせ</div></a>
				<a href="" class="question_contact_button question_contact_button--mail"><div class="question_contact_button_inner">問い合わせフォーム</div></a>
				<a href="" class="question_contact_button question_contact_button--tel"><div class="question_contact_button_inner">お電話で問い合わせ</div></a>
			</div>
		</div>
		
		<div class="section section--order">
			<div class="section_head">
				<h2 class="section_head_title">宅配買取お申込み～ご入金までの流れ</h2>				
			</div>      
			<ul class="step_list">
				<li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.01</div>
            <div class="step_image"></div>
            <div class="step_detail">
              <h3 class="step_detail_title">買取フォームから申込み</h3>
              <p class="step_detail_text">下記のボタンより、買い取り申し込みフォームに必要事項をご記入ください。宅配キットをお送りいたしいます。宅配キットは無料ですのでお気軽にご利用ください。</p>
              <div class="order_button_wrap"><a href="" class="order_button">買い取り申し込みはコチラ</a></div>
            </div>
          </div>
        </li>
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.02</div>
            <div class="step_image"></div>
            <div class="step_detail">
              <h3 class="step_detail_title">ご自宅に宅配キットが到着</h3>
              <p class="step_detail_text">宅配キットが到着いたしましたら、「買取申込用紙・着払い伝票」が同梱されているかご確認ください。</p>
            </div>
          </div>
        </li>
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.03</div>
            <div class="step_image"></div>
            <div class="step_detail">
              <h3 class="step_detail_title">買取希望のお品を梱包する</h3>
              <p class="step_detail_text">
              買取希望のお品物を緩衝材などで包み、箱に詰めて発送準備をしてください。お買い取り受付時のご確認事項として、買取規定もご確認ください。また、高額査定ブランド、高額査定のコツをコチラのページでご確認いただけます。</p>
              <p class="step_detail_notice">
                【送付して頂くもの】<br>
                ・査定を依頼されるお品物<br>
                ・ご本人様確認書類：運転免許証や保険証、パスポートのコピーで結構です。<br>
                ・買取申込用紙（宅配キットに入っています）</p>          
            </div>
          </div>
        </li>
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.04</div>
            <div class="step_image"></div>
            <div class="step_detail">
              <h3 class="step_detail_title">運送業者に集荷依頼をかけ、弊社までご発送ください。</h3>
              <p class="step_detail_text">ご発送は宅配キットに同封の着払い伝票（佐川急便）をご利用の上、お客さまの地域の佐川急便営業所までご連絡ください。こちらで営業所を検索できます。</p>
              <p>※お客様の送料ご負担は「0円」です！</p>
            </div>
          </div>
        </li>
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.05</div>
            <div class="step_image"></div>
            <div class="step_detail">
              <h3 class="step_detail_title">商品到着・査定・ご連絡</h3>
              <p class="step_detail_text">弊社にお品物が届き次第、直ちに専門のバイヤーが一点一点丁寧に査定し、ご連絡いたします。万が一、査定額にご納得いただけない場合は、送料弊社負担にてお品を返送させていただきます。※到着分のお品物の量が多い場合やスーパーブランド等の高額品の場合は少々お時間をいただく場合がございます。査定をお急ぎの場合はご一報ください。</p>
            </div>
          </div>
        </li>
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.06</div>
            <div class="step_image"></div>
            <div class="step_detail">
              <h3 class="step_detail_title">買い取り成立の場合</h3>
              <p class="step_detail_text">査定額にご満足いただけますと、ご了承連絡の翌営業日にご希望の方法で入金させていただきます。 もちろん振込み手数料も当社が負担いたします！※お買取り成立が土日祝の場合、ご入金は翌銀行営業日となります。</p>
              <h3 class="step_detail_title">買い取り不成立の場合</h3>
              <p class="step_detail_text">査定額がご希望に添えなかった場合なかった場合、ご連絡後３日以内に商品を返送させて頂きます。もちろん送料は当社が負担致しますので、お客様負担はございません。また、査定料金や手数料といったものは一切いただきません。ご安心ください。</p>
              <p class="step_detail_notice--red">※最初にメール査定結果を送らせて頂いてから1ヶ月間ご連絡がなかったり、買取成立の可否が確定しない場合は、返送させて頂きます。</p>
            </div>
          </div>
        </li>
			</ul>
      <div class="button_wrap"><a href="#" class="button--large">買い取り申し込みはこちら</a></div>
		</div><!-- [end] .section -->
    
		<div class="section section--tips">
      <div class="section_head">
				<h2 class="section_head_title"><i>出荷前にCHECK！</i><br>査定額UPの為の3つのコツ</h2>				
			</div>			
			<div class="kaitori_point">
				<ul class="point_list">
					<li class="point_list_item point_list_item--point01">
						<h3 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_01.png" alt="01"></span>
              <span class="point_list_item_title_text">状態によって査定額が上下します。</span>
            </h3>
						<p class="point_list_item_lead">エリ、袖等の汚れによって査定額は上下します。クリーニングに出していただく必要はございませんが、ご家庭でお洗濯をしていただくことをオススメします。</p>
					</li>
					<li class="point_list_item point_list_item--point02">
						<h3 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_02.png" alt="02"></span>
              <span class="point_list_item_title_text">付属品（タグ・箱・説明書など）がついているとUP!</span>
            </h3>
						<p class="point_list_item_lead">箱やタグなどの状態の良い付属品がついていると、査定額がUPする可能性が高いです！また、一部商品では付属品やタグが無いと買取できない商品もございます。詳しくは 買取規定 をご確認ください。</p>
					</li>
					<li class="point_list_item point_list_item--point03">
						<h3 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_03.png" alt="03"></span>
              <span class="point_list_item_title_text">今シーズン物でUP!</span>
            </h3>
						<p class="point_list_item_lead">新しいシーズンの商品ほど値段がつきやすくなっております！</p>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="section section--brand">
    <?php
    require_once (dirname(__FILE__) . '/assets/include/brand_list.php');
    ?>
		</div>
		<div class="section section--contact">
      <div class="button_wrap">
        <a href="#" class="button--order"><img src="./assets/images/kaitori_delivery/btn_txt_order.png" alt="買い取り申し込みはこちら24時間受付"></a>
        <div class="button--contact for-PC" ><img src="./assets/images/kaitori_delivery/btn_txt_contact.png" alt="お電話でお問い合わせ TEL.06-6281-4567 受付時間 11:00〜20:00/水曜日定休"></div>
        <a href="tel:06-6281-4567" class="button--contact for-SP"><img src="./assets/images/kaitori_delivery/btn_txt_contact.png" alt="お電話でお問い合わせ TEL.06-6281-4567 受付時間 11:00〜20:00／水曜日定休"></a>
      </div>			
		</div>
    <div class="kaitori_banner double">
			<ul class="kaitori_banner_list double">
				<li class="kaitori_banner_list_item kaitori_banner_list_item--shop">
					<a href=""><img src="./assets/images/btn_kaitori_shop_txt.png" alt="店頭買取について"></a>
				</li>
				<li class="kaitori_banner_list_item kaitori_banner_list_item--line">
					<a href=""><img src="./assets/images/btn_kaitori_line_txt.png" alt="かんたん！LINE査定"></a>
				</li>
			</ul>
		</div>
	</div><!-- [end] .page_body -->
</div><!-- [end] .main -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>