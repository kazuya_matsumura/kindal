<?php
define ('PAGE_CAT' , 'article');
define ('PAGE_ID' , 'result');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '高価買取実績｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
  
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">高価買取実績</h1>
    </div>
  </div>
  
	<div class="page_body">
    <div class="article_wrap">    
      <div class="article_list">
        
        <article class="">          
          <header>
            <time>2019.10.01</time>
            <h2>MONTBLANC／モンブラン</h2>
          </header>
          <section class="intro">
            <figure><img src="./assets/images/article/article_img_dummy.jpg"></figure>				
            <p>【MONTBLANC／モンブラン】の宅配買取は簡単・安心送料無料の【心斎橋】カインドオル東心斎橋店にお任せください！全ての【MONTBLANC/モンブラン】製品をお客様のご期待にお応えできるよう丁寧な買取をさせて頂きます。まとめて売るならお得な20％UPサービス実施中！古着・靴・バッグ・時計など高額査定。LINE査定実施中。大阪/東心斎橋実店舗は夜11時まで営業中！店頭買取も大歓迎♪</p>
          </section>
          <section>
            <h3>h3見出しh3見出しh3見出しh3見出し</h3>				
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          </section>
          <section>
            <h4>h4見出しh4見出しh4見出しh4見出しh4見出し</h4>
            <h5>h5見出しh5見出しh5見出しh5見出しh5見出しh5見出し</h5>
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          </section>
          <div class="kaitori_banner">
            <?php require (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
          </div>
        </article>

        <article class="">          
          <header>
            <time>2019.10.01</time>
            <h2>MONTBLANC／モンブラン</h2>
          </header>
          <section class="intro">
            <figure><img src="./assets/images/article/article_img_dummy.jpg"></figure>				
            <p>【MONTBLANC／モンブラン】の宅配買取は簡単・安心送料無料の【心斎橋】カインドオル東心斎橋店にお任せください！全ての【MONTBLANC/モンブラン】製品をお客様のご期待にお応えできるよう丁寧な買取をさせて頂きます。まとめて売るならお得な20％UPサービス実施中！古着・靴・バッグ・時計など高額査定。LINE査定実施中。大阪/東心斎橋実店舗は夜11時まで営業中！店頭買取も大歓迎♪</p>
          </section>
          <section>
            <h3>h3見出しh3見出しh3見出しh3見出し</h3>				
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          </section>
          <section>
            <h4>h4見出しh4見出しh4見出しh4見出しh4見出し</h4>
            <h5>h5見出しh5見出しh5見出しh5見出しh5見出しh5見出し</h5>
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          </section>
          <div class="kaitori_banner">
            <?php require (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
          </div>
        </article>

        <article class="">          
          <header>
            <time>2019.10.01</time>
            <h2>MONTBLANC／モンブラン</h2>
          </header>
          <section class="intro">
            <figure><img src="./assets/images/article/article_img_dummy.jpg"></figure>				
            <p>【MONTBLANC／モンブラン】の宅配買取は簡単・安心送料無料の【心斎橋】カインドオル東心斎橋店にお任せください！全ての【MONTBLANC/モンブラン】製品をお客様のご期待にお応えできるよう丁寧な買取をさせて頂きます。まとめて売るならお得な20％UPサービス実施中！古着・靴・バッグ・時計など高額査定。LINE査定実施中。大阪/東心斎橋実店舗は夜11時まで営業中！店頭買取も大歓迎♪</p>
          </section>
          <section>
            <h3>h3見出しh3見出しh3見出しh3見出し</h3>				
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          </section>
          <section>
            <h4>h4見出しh4見出しh4見出しh4見出しh4見出し</h4>
            <h5>h5見出しh5見出しh5見出しh5見出しh5見出しh5見出し</h5>
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          </section>
          <div class="kaitori_banner">
            <?php require (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
          </div>
        </article>

        <div class="pagenation">
          <ul class="pagenation_list">
            <li><a class="progress" href="#">1/3ページ</a></li>
            <li><span class="current">1</span></li>
            <li><a class="num" href="#">2</a></li>
            <li><a class="num" href="#">3</a></li>
          </ul>
        </div>

      </div><!-- [end] .article_list -->

      <?php require (dirname(__FILE__) . '/assets/include/sidemenu.php'); ?>
      
    </div><!-- [end] .article_wrap -->    
	</div><!-- [end] .page_ody -->
</div><!-- [end] .main--article -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>