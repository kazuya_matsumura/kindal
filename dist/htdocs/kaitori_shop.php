<?php
define ('PAGE_CAT' , 'subpage');
define ('PAGE_ID' , 'kaitori_shop');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '店頭買取｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">店頭買取</h1>
    </div>
  </div>
	<div class="page_body">
		<div class="section section--intro">
			<p class="intro_lead">高価買取、売って感動！<br>ブランド古着を大阪心斎橋店にお売りください！</p>
		</div>	
		<div class="section section--point">
			<div class="section_head">
				<h2 class="section_head_title">店頭買取3つのポイント</h2>
			</div>
			<ul class="point_list">
				<li class="point_list_item point_list_item--point01">
					<p class="kaitori_point_number"><img src="./assets/images/point_01.png" alt="01"></p>
					<figure class="kaitori_point_thumb"><img src="./assets/images/kaitori_shop/point_img01.jpg" alt="出来るだけ、お売りになる方の立場でお買い取り！"></figure>
					<div class="point_list_item_title">
						<h3 class="point_list_item_title_lead">出来るだけ、お売りになる方の立場でお買い取り！</h3>
						<ul class="point_list_item_lead_list">
							<li>出来るだけ高く買い取ります！</li>
							<li>次の人が喜んで買っていただけたことをお伝えする様にしています！</li>
							<li>大切に取り扱います！</li>
						</ul>
					</div>				
				</li>
				<li class="point_list_item point_list_item--point02">
					<p class="kaitori_point_number"><img src="./assets/images/point_02.png" alt="02"></p>
					<figure class="kaitori_point_thumb"><img src="./assets/images/kaitori_shop/point_img02.jpg" alt="買取も、信用が一番大切！"></figure>
					<div class="point_list_item_title">
						<h3 class="point_list_item_title_lead">買取も、信用が一番大切！</h3>
						<ul class="point_list_item_lead_list">
							<li>創業1985年　30余年の長い実績！</li>
							<li>店舗数　全国　35店舗以上！　今も拡大中！</li>
							<li>東心斎橋店は夫婦で顔の見える丁寧な接客！</li>
						</ul>
					</div>
				</li>
				<li class="point_list_item point_list_item--point03">
					<p class="kaitori_point_number"><img src="./assets/images/point_03.png" alt="03"></p>
					<figure class="kaitori_point_thumb"><img src="./assets/images/kaitori_shop/point_img03.jpg" alt="アウトドア、アメカジブランドが大好き！"></figure>
					<div class="point_list_item_title">
						<h3 class="point_list_item_title_lead">アウトドア、アメカジブランドが大好き！</h3>
						<ul class="point_list_item_lead_list">
							<li>パタゴニア、ノースフェイスなどアウトドアブランドは特に高く買い取っています！</li>
							<li>エンジニアガーメンツ、東洋エンタープライズなどのアメカジブランド大募集中です！</li>
							<li>ブログで買取＆販売のご紹介！</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>  
		<div class="section section--flow">
			<div class="section_head">
				<h2 class="section_head_title">店舗買取の流れ</h2>
			</div>	
			<ul class="kaitoriFlow_list">
				<li class="kaitoriFlow_list_item">
					<h3 class="kaitoriFlow_list_item_title">01</h3>
					<figure class="kaitoriFlow_list_item_thumb"><img src="./assets/images/kaitori_shop/flow_img01.jpg" alt="1点からでも大歓迎、高額査定！"></figure>
					<p class="kaitoriFlow_list_item_text">1点からでも大歓迎、<br>高額査定！</p>
				</li>
				<li class="kaitoriFlow_list_item">
					<h3 class="kaitoriFlow_list_item_title">02</h3>
					<figure class="kaitoriFlow_list_item_thumb"><img src="./assets/images/kaitori_shop/flow_img02.jpg" alt="すぐに！丁寧に！商品を査定"></figure>
					<p class="kaitoriFlow_list_item_text">すぐに！丁寧に！<br>商品を査定！</p>
				</li>
				<li class="kaitoriFlow_list_item">
					<h3 class="kaitoriFlow_list_item_title">03</h3>
					<figure class="kaitoriFlow_list_item_thumb"><img src="./assets/images/kaitori_shop/flow_img03.jpg" alt="ご提示金額でご納得いただければ！その場ですぐにお支払！"></figure>
					<p class="kaitoriFlow_list_item_text">ご提示金額で<br>ご納得いただければ！<br>その場ですぐにお支払！</p>
				</li>
				<li class="kaitoriFlow_list_item">
					<h3 class="kaitoriFlow_list_item_title">04</h3>
					<figure class="kaitoriFlow_list_item_thumb"><img src="./assets/images/kaitori_shop/flow_img04.jpg" alt="お買取りついでに充実商品をチェック！"></figure>
					<p class="kaitoriFlow_list_item_text">お買取りついでに<br>充実商品をチェック！</p>
				</li>
			</ul>
		</div>
		<div class="kaitori_banner double">
			<ul class="kaitori_banner_list double">
				<li class="kaitori_banner_list_item kaitori_banner_list_item--delivery">
					<a href=""><img src="./assets/images/btn_kaitori_delivery_txt.png" alt="宅配買取について"></a>
				</li>
				<li class="kaitori_banner_list_item kaitori_banner_list_item--line">
					<a href=""><img src="./assets/images/btn_kaitori_line_txt.png" alt="かんたん！LINE査定"></a>
				</li>
			</ul>
		</div>
		<div class="section--end">
			<p class="end_text">買取持込はこちらへ！</p>
			<p class="end_arrow"><img src="./assets/images/arrow_down.png"></p>
		</div>
	</div><!-- [end] .page_body -->
</div><!-- [end] .main -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>