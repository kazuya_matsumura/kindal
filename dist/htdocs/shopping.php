<?php
define ('PAGE_CAT' , 'subpage');
define ('PAGE_ID' , 'shopping');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
  
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">Kindal東心斎橋<br>ショッピングページ</h1>
    </div>
  </div>
  <div class="page_body">    
    <div class="section section--product">
      <div class="section_head">
        <h2 class="section_head_title">販売商品</h2>
      </div>
      <ul class="product_list">
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">アウター</span>
              <span class="product_list_item_text_en">Outer</span>    
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_outer.jpg" alt="アウター">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">トップス</span>
              <span class="product_list_item_text_en">Tops</span>  
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_tops.jpg" alt="トップス">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">パンツ</span>
              <span class="product_list_item_text_en">Pants</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_pants.jpg" alt="パンツ">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">時計</span>
              <span class="product_list_item_text_en">Watch</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_watch.jpg" alt="時計">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">バッグ</span>
              <span class="product_list_item_text_en">Bag</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_bag.jpg" alt="バッグ">  
            </div>
          </div>         
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">小物/財布</span>
              <span class="product_list_item_text_en">Accessories / Wallet</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_accessories_wallet.jpg" alt="小物/財布">  
            </div>
          </div>         
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">メガネ</span>
              <span class="product_list_item_text_en">Glasses</span>    
           </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_glasses.jpg" alt="メガネ">
            </div>
          </div>          
        </li>           
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">靴</span>
              <span class="product_list_item_text_en">Shoes</span>  
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_shoes.jpg" alt="靴">
            </div>
          </div>          
        </li>           
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">帽子</span>
              <span class="product_list_item_text_en">Hat</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_hat.jpg" alt="帽子">
            </div>
          </div>          
        </li>         
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">オリジナル</span>
              <span class="product_list_item_text_en">Original</span>   
           </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_original.jpg" alt="オリジナル">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">その他</span>
              <span class="product_list_item_text_en">Other</span>      
            </div>
          </div> 
        </li>
      </ul>
    </div> <!-- [end] .section --> 

    <div class="section section--strong">
      <div class="section_head">
        <h2 class="section_head_title">Kindalの強み</h2>
      </div>
      <div class="strong_point">			
        <ul class="point_list">
          <li class="point_list_item point_list_item--point01">
            <h3 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_01.png" alt="01"></span>
              <span class="point_list_item_title_text">すべてメンテナンスをして<br>出品しております</span>								
            </h3>
            <p class="point_list_item_lead">メンテナンスを一切しない業者も多いですが当店は基本中古品ですので全てメンテナンスをして出品しております。</p>
          </li>
          <li class="point_list_item point_list_item--point02">
            <h3 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_02.png" alt="02"></span>
              <span class="point_list_item_title_text">できる限りのお買い得な価格<br>で販売しております</span>
            </h3>
            <p class="point_list_item_lead">多くのお客様に喜んでいただけるように中古相場をよく調べて、できる限りお買得な価格で販売しております。</p>
          </li>
        </ul>
      </div>
    </div><!-- [end] .section -->
    
    <div class="section section--recommended">
      <div class="section_head">
        <h2 class="section_head_title">オススメの商品</h2>
      </div>
      <ul class="product_list">
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">      
              <span class="product_list_item_text_name">ブルーナボイン</span>
              <span class="product_list_item_text_en">BRU NA BOINNE</span>
            </div>
            <div class="product_list_item_thumb">        
              <img src="./assets/images/shopping/th_bru_na_boinne.jpg" alt="">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">児島ジーンズ</span>
              <span class="product_list_item_text_en">KOJIMA GENES</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_kojima_genes.jpg" alt="">
            </div>
          </div>          
        </li> 
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">ボーター</span>
              <span class="product_list_item_text_en">PORTER</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_porter.jpg" alt="">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">マグナーニ</span>
              <span class="product_list_item_text_en">MAGUNANNI</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_magunanni.jpg" alt="">
            </div>
          </div>          
        </li> 
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">パタゴニア</span>
              <span class="product_list_item_text_en">Patagonia</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_patagonia.jpg" alt="">
            </div>
          </div>          
        </li> 
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">ザ・ノース・フェイス</span>
              <span class="product_list_item_text_en">THE NORTH FACE</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_the_north_face.jpg" alt="">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">ミリタリー</span>
              <span class="product_list_item_text_en">Military</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_military.jpg" alt="">
            </div>
          </div>          
        </li>
        <li class="product_list_item">
          <div class="product_list_item_inner">
            <div class="product_list_item_text">
              <span class="product_list_item_text_name">US古着</span>
              <span class="product_list_item_text_en">US old clothes</span>
            </div>
            <div class="product_list_item_thumb">
              <img src="./assets/images/shopping/th_us_old_clothes.jpg" alt="">
           </div>
          </div>          
        </li>
      </ul>
      <div class="button_wrap"><a href="" class="button--large">ヤフオク！ショッピングページへ</a></div>
    </div><!-- [end] .section -->
    
    <div class="section section--original">
      <h1 class="original_title">東心斎橋店オリジナル商品</h2>
      <div class="section_head">
        <h3 class="section_head_title">販売商品</h3>
      </div>
      <div class="original_product">
        <div class="original_product_item">
          <h4 class="original_product_item_head">天然ダイヤ使用18金<br>ホースシューリング</h4>
          <div class="original_product_item_image"><img src="./assets/images/shopping/original_img01.jpg" alt="天然ダイヤ使用18金 ホースシューリング"></div>
        </div>
        <div class="original_product_item">
          <h4 class="original_product_item_head">天然ダイヤ使用シルバー925<br>ホースシューリング</h4>
          <div class="original_product_item_image"><img src="./assets/images/shopping/original_img02.jpg" alt="天然ダイヤ使用シルバー925 ホースシューリング"></div>
        </div>
      </div>
      <div class="original_point">			
        <ul class="point_list">
          <li class="point_list_item point_list_item--point01">
            <h4 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_01.png" alt="01"></span>
              <span class="point_list_item_title_text">当店でのみ販売<br>しております</span>								
            </h3>
            <p class="point_list_item_image"><a href="https://shop.kind.co.jp/" target="_blank"><img src="./assets/images/shopping/original_img03.jpg" alt="カインドオル　オンラインショップ"></a></p>
          </li>
          <li class="point_list_item point_list_item--point02">
            <h4 class="point_list_item_title">
              <span class="point_list_item_title_image"><img src="./assets/images/point_02.png" alt="02"></span>
              <span class="point_list_item_title_text">日本の老舗ジュエリー工房<br>オリジナル品です</span>
            </h3>
            <p class="point_list_item_image"><img src="./assets/images/shopping/original_img04.jpg" alt="日本の老舗ジュエリー工房オリジナル品です"></p>
          </li>
        </ul>
      </div>
      <div class="button_wrap"><a href="#" class="button--large">ヤフオク！ショッピングページへ</a></div>
    </div><!-- [end] .section -->

    
    <div class="section section--original_about">
      <div class="section_head">
        <h2 class="section_head_title">オリジナル商品について</h2>
      </div>
      <ul class="post_list">
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="https://placehold.jp/200x200.png?text={記事サムネイル}" alt="記事サムネイル"></div>
            <p class="post_list_item_text">記事タイトル</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="https://placehold.jp/200x200.png?text={記事サムネイル}" alt="記事サムネイル"></div>
            <p class="post_list_item_text">記事タイトル</p>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="https://placehold.jp/200x200.png?text={記事サムネイル}" alt="記事サムネイル"></div>
            <p class="post_list_item_text">記事タイトル</p>
          </a>
        </li>
      </ul>
      <div class="button_wrap"><a href="" class="button--more">オリジナル商品の記事一覧</a></div>
    </div><!-- [end] .section--original_about -->
  </div><!-- [end] .page_body -->
</div><!-- [end] .main -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>