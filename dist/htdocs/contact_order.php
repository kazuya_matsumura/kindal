<?php
define ('PAGE_CAT' , 'contact');
define ('PAGE_ID' , 'order');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '');
?>
<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">買取申込み<br>受付フォーム</h1>
    </div>
  </div>
  <div class="page_body">    
    <div class="section section--intro">
      <p class="contact_intro_text">下記フォームに必要事項を入力後、送信ボタンを押してください。</p>
    </div>

    <div class="section section--form">
      <form id="" name="" method="" autocomplete="">
        <div class="form_body">

          <div class="form_item">
            <label for="" class="form_item_title">お名前<span class="required">必須</span></label>          
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />
          </div>
          <div class="form_item">
            <label for="" class="form_item_title">メールアドレス<span class="required">必須</span></label>
            <input type="email" name="" id="" class="form_item_input_text" placeholder="" value="" />          
          </div>
          <div class="form_item">
            <label for="" class="form_item_title">お電話番号<span class="required">必須</span></label>
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />         
          </div>          
          <div class="form_item">
            <h2 class="form_item_title">性別</h2>
            <div class="form_item_input_wrap">
              <label class="form_item_input_radio"><input type="radio" name="your-gender" value="男" /><span class="">男</span></label>
              <label class="form_item_input_radio"><input type="radio" name="your-gender" value="女" /><span class="">女</span></label>
            </div>            
          </div>
          <div class="form_item">
            <label for="" class="form_item_title">お見積りご依頼内容<span class="required">必須</span></label>
            <div class="form_item_input_wrap">
              <textarea name="quote" cols="40" rows="10" class="form_item_input_textarea" aria-required="true" aria-invalid="false">ブランド名（　　　　　　　/　　点）&#010;ブランド名（　　　　　　　/　　点）&#010;ブランド名（　　　　　　　/　　点）&#010;ブランド名（　　　　　　　/　　点）</textarea>
              <p><span>例</span>ギャルソン/4点</p>
            </div>            
          </div>

          <div class="form_item">
            <label for="" class="form_item_title">郵便番号</label>
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />        
          </div>
          <div class="form_item">
            <label for="" class="form_item_title">ご住所<span class="required">必須</span></label>
            <input type="text" name="" id="" class="form_item_input_text" placeholder="" value="" />
          </div>

          <fieldset>
            <legend>宅配キットのお申込み</legend>
            <div class="form_item">
              <label for="" class="form_item_title">宅配キット</label>
              <div class="form_item_input_wrap">
                <label class="form_item_input_radio"><input type="radio" name="delivery-type" value="着払い伝票のみ" /><span class="">着払い伝票のみ</span></label>
                <label class="form_item_input_radio"><input type="radio" name="delivery-type" value="着払い伝票＋梱包用段ボール小(100サイズ/デニム最大12本）" /><span class="">着払い伝票＋梱包用段ボール小(100サイズ/デニム最大12本）</span></label>
                <label class="form_item_input_radio"><input type="radio" name="delivery-type" value="着払い伝票＋梱包用段ボール大(140サイズ/デニム最大30本）" /><span class="">着払い伝票＋梱包用段ボール大(140サイズ/デニム最大30本）</span></label>
              </div>
            </div>

            <div class="form_item">
              <label for="" class="form_item_title">宅配キットの個数</label>
              <div class="form_item_input_wrap">
                <input type="number" name="delivery-number" value="" class="form_item_input_number" min="1" max="5" aria-invalid="false" />個
                <p class="form_item_input_read">平日12時までにお申し込みいただきますと最短で翌日のお届けが可能です。<br />
                  ※お住まいの地域によっては、ご希望に添えない場合がございます。<br />
              ※一度にお申込みいただける段ボールの個数は5個までとなっております。</p>
              </div>              
            </div>

            <div class="form_item">
              <label for="" class="form_item_title">宅配キットの到着時間指定</label>
              <div class="form_item_input_wrap">
                <select name="delivery-time" class="form_item_input_select" aria-invalid="false">
                  <option value="指定なし">指定なし</option>
                  <option value="午前中">午前中</option>
                  <option value="12時～14時">12時～14時</option>
                  <option value="14時～16時">14時～16時</option>
                  <option value="16時～18時">16時～18時</option>
                  <option value="18時～21時">18時～21時</option>
                </select>
              </div>
            </div>

            <div class="form_item">
              <label for="" class="form_item_title">宅配キットの到着日指定</label>
              <div class="form_item_input_wrap">
                <input type="date" name="delivery-date" value="" class="form_item_input_date" min="" max="" aria-invalid="false" />
                <p class="form_item_input_read">入力欄の▼を<span class="for-PC">クリック</span><span class="for-SP">タップ</span>するとカレンダーが表示されます。カレンダーからご希望日をご選択ください。</p>
              </div>
            </div>
          </fieldset>

          <p class="caption">20歳以下の未成年の方はご利用することができません。</p>

          <div class="button_wrap"><input type="submit" value="送信する" class="form_button submit" /></div>
    
        </div><!-- [end] .form_body -->

        <div class="wpcf7-response-output wpcf7-display-none"></div><!-- 動作メッセージ -->
        
      </form>        
    </div><!-- [end] .section_form -->
  </div><!-- [end] .page_body -->
</div><!-- [end] .main -->
  
<?php
include_once (dirname(__FILE__) . '/assets/include/footer.php');
?>