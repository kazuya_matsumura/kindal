<?php
define ('PAGE_CAT' , 'category');
define ('PAGE_ID' , 'target');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>

<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">買取対象商品</h1>
    </div>
  </div>
  <div class="page_body">
    <div class="section section--brand">
      <div class="section_head">
        <h2 class="section_head_title">ブランド一覧</h2>
      </div>
      <?php include_once (dirname(__FILE__) . '/assets/include/brand_list_full.php'); ?>
    </div>
    
    
  	<div class="section section--post">
      <ul class="post_list">
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb04.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">アウター</p>
              <p>ダウンジャケットの定番ブランドカナダグースの人気アイテムといえば　ジャスパー/JASPER！</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb05.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">バッグ</p>
              <p>ビジネスバッグの定番！ポーター</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb06.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">メガネ</p>
              <p>エフェクター/EFFECTORの魅力について</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb04.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">アウター</p>
              <p>ダウンジャケットの定番ブランドカナダグースの人気アイテムといえば　ジャスパー/JASPER！</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb05.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">バッグ</p>
              <p>ビジネスバッグの定番！ポーター</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb06.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">メガネ</p>
              <p>エフェクター/EFFECTORの魅力について</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb04.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">アウター</p>
              <p>ダウンジャケットの定番ブランドカナダグースの人気アイテムといえば　ジャスパー/JASPER！</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb05.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">バッグ</p>
              <p>ビジネスバッグの定番！ポーター</p>
            </div>
          </a>
        </li>
        <li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/category/dummy_thumb06.jpg" alt=""></div>
            <div class="post_list_item_text">
              <p class="post_list_item_text_genre">メガネ</p>
              <p>エフェクター/EFFECTORの魅力について</p>
            </div>
          </a>
        </li>
      </ul>
      
      <div class="pagenation">
        <ul class="pagenation_list">
          <li><a class="progress" href="#">1/3ページ</a></li>
          <li><span class="current">1</span></li>
          <li><a class="num" href="#">2</a></li>
          <li><a class="num" href="#">3</a></li>
        </ul>
      </div>      
    </div><!-- [end] .section -->
	
	  <div class="section">
      <div class="kaitori_banner">
        <?php require_once (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
      </div>
    </div><!-- [end] .section -->
    
  </div><!-- [end] .page_body -->
</div><!-- [end] .main -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>