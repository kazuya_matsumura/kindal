<?php
define ('PAGE_CAT' , 'subpage');
define ('PAGE_ID' , 'kaitori_line');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , 'LINE査定｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">LINE査定</h1>
    </div>
  </div>
  <div class="page_body">
    <div class="section section--step">
      <div class="section_head">
        <h2 class="section_head_title">LINE査定かんたん3ステップ！</h2>
      </div>
      <ul class="step_list">  	
				<li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.01</div>
            <div class="step_image"><img src="assets/images/kaitori_line/step_img01.jpg" alt=""></div>
            <div class="step_detail">
              <h3 class="step_detail_title">撮る</h3>
              <p class="step_detail_text">明るい場所でピントを合わせて、商品全体を画面に収めてください。</p>
            </div>
          </div>
        </li>
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.02</div>
            <div class="step_image"><img src="assets/images/kaitori_line/step_img02.jpg" alt=""></div>
            <div class="step_detail">
              <h3 class="step_detail_title">送る</h3>
              <p class="step_detail_text">お品物の正確な情報をお送りいただければ、よりスムーズな買取につながります。入力には【テンプレート】をご利用ください。<br>お品物の情報で、不明な部分は未記入でも問題ありません。<br>タグの表記などあれば、写真でもお送りください。査定の参考とさせていただきます。</p>
              <p class="step_detail_notice">
                【テンプレート】<br>
                メーカー：<br>
                商品名　：<br>
                商品状態（新品・中古）：<br>
                サイズ　：<br>
                製造番号・品番：<br>
                付属品　：<br>
                カラー　：<br>
                箱・保証書：<br>
                ご購入年月：<br>
                ご購入時の金額：<br>
                備考　：</p>          
            </div>
          </div>
        </li>  
        <li class="step_list_item">
          <div class="step_marker"></div>
          <div class="step_wrap">
            <div class="step_number">Step.03</div>
            <div class="step_image"><img src="assets/images/kaitori_line/step_img03.jpg" alt=""></div>
            <div class="step_detail">
              <h3 class="step_detail_title">査定くる</h3>
              <p class="step_detail_text">詳細など多く送っていただければ、取引がスムーズになります。</p>              
          </div>
        </li>
			</ul>
      <div class="line_regist">
				<a href="" class="line_regist_button">
          <div class="line_regist_button_inner">
            LINE登録はこちら<br>
            <span>LINE ID: @oau7488f</span>
          </div>
        </a>
			</div>
    </div><!-- [end] .section--step -->
	
    <div class="section section--qr">
      <div class="section_head">
        <h2 class="section_head_title">QRコードでの登録はこちら</h2>
      </div>
      <div class="qr">
        <figure class="qr_code"><img src="assets/images/kaitori_line/qr_img01.png" alt="QRコード"></figure>
        <figure class="qr_flow for-PC"><img src="assets/images/kaitori_line/qr_img02.jpg" alt="LINEを開いて、友だち追加をタップ→QRコードをタップ→QRコードをカメラで撮る→友だちリストに追加をタップ"></figure>
        <ul class="qr_flow_wrap for-SP">
          <li><img src="assets/images/kaitori_line/qr_flow01.png" alt="LINEを開いて、友だち追加をタップ"></li>
          <li><img src="assets/images/kaitori_line/qr_flow02.png" alt="QRコードをタップ"></li>
          <li><img src="assets/images/kaitori_line/qr_flow03.png" alt="QRコードをカメラで撮る"></li>
          <li><img src="assets/images/kaitori_line/qr_flow04.png" alt="友だちリストに追加をタップ"></li>
        </ul> 
      </div>      
    </div><!-- [end] .section--qr-->
    
    <div class="section section--brand">
      <?php require_once (dirname(__FILE__) . '/assets/include/brand_list.php'); ?>
    </div>
    <div class="section section--message">
      <p>商品写真をLINEで送ってください。詳細が分かる別カットなども送ってください。</br></br>
  ブランド名、品番、サイズ、購入年、商品状態などについて詳しいコメントをください。または写真でもOKです。</br></br>
  査定額はあくまでも参考価格となります。画像やコメントが詳しいほどより正確な査定ができますのでよろしくお願い致します。</br></br>
  買取価格は実際に見て変動する可能性があります。</br></br>
  ご理解よろしくお願い致します。</p>		
    </div>
     <div class="kaitori_banner double">
			<ul class="kaitori_banner_list double">
				<li class="kaitori_banner_list_item kaitori_banner_list_item--shop">
					<a href=""><img src="./assets/images/btn_kaitori_shop_txt.png" alt="店頭買取について"></a>
				</li>
				<li class="kaitori_banner_list_item kaitori_banner_list_item--line">
					<a href=""><img src="./assets/images/btn_kaitori_delivery_txt.png" alt="宅配買取について"></a>
				</li>
			</ul>
		</div>
	</div><!-- [end] .page_body -->
</div><!-- [end] .main -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>