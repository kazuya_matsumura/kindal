<?php
define ('PAGE_CAT' , 'subpage');
define ('PAGE_ID' , 'faq');
define ('PAGE_DESC' , '');
define ('PAGE_TITLE' , '｜大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
  <div class="page_head">
    <div class="page_head_inner">
      <h1 class="page_head_title">よくあるご質問</h1>
    </div>
  </div>
  <div class="page_body">
    <div class="section">
      <dl class="faq_list">
        <dt class="faq_list_question"><i>Q</i>1点ずつ金額をつけてもらえるのでしょうか？</dt>
        <dd class="faq_list_answer"><i>A</i>基本的には店頭販売可能と判断いたしましたお品は単品でお値段をつけさせて頂いております。ただし、ブランドの需要・状態・年式・デザイン等を考慮した結果、おまとめでの査定となる場合もございます。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>どういったブランドの品物が高く評価されるのでしょうか？</dt>
        <dd class="faq_list_answer"><i>A</i>まず第一に該当ブランドの中古市場での需要が重要となります。ブランドを代表するアイコン的なアイテムやプロパー店で品薄状態のブランドなどは高価買い取りにつながりやすいです。コアなブランドでも取扱店なども考慮したうえで経験豊富な知識を持ったバイヤーが査定いたしますので”間違いのない査定”が可能となっております。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>高く買い取ってもらうためのコツはありますか？</dt>
        <dd class="faq_list_answer"><i>A</i>評価のウェイトですが、大まかにですが、状態・ブランド＞デザイン（色なども含む）＞年式＞サイズ　といった流れで評価が決まります。売れ筋ブランドでも状態によっては評価が下がることがございます。逆に上の評価基準をクリアしたお品は高額査定が期待できます。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>キャンセルは可能でしょうか？</dt>
        <dd class="faq_list_answer"><i>A</i>もちろん可能です。一部をキャンセル、その他は買取といったことも可能です。<br>
        <span class="caption">※おまとめで査定させて頂いたお品につきましては全品買取か全品返却かのいずれかの選択となります。またキャンセルの際の返送料などは弊社で負担させて頂いております。お客様には一切料金は発生いたしません。</span></dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>査定までの日数・入金までどのくらいかかりますか？</dt>
        <dd class="faq_list_answer"><i>A</i>弊社にお品が到着してから最短当日に査定結果をご連絡をさせて頂いております。目安としてお申し込み画面に査定日数を掲示しております。目安なので早く査定が完了する事も多々ございます。「申込フォーム」<br>  原則として買取が成立した翌日のお振込み対応となります。（土日祝は金融機関の休業日の為翌営業日のお振込みとなります）<br><span class="caption">※全てメールでご連絡差し上げておりますが、まれにメールが届かないケースがございます。 お品物到着後3日経過しても連絡がない場合、何らかのエラーも考えられますのでご連絡頂きましたら、早急にご対応させて頂きます。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>買取業者が多数あり、どこを選んだらいいかわからない</dt>
        <dd class="faq_list_answer"><i>A</i>現在ブランド古着を取り扱う業者は年々増加傾向にございます。その中で特に買取をご利用されたことがないお客様はネットの掲示板などの情報から弊社を含め買取業者にたどり着くことも少なくないかと思います。ただしネットの情報は非常にあいまいなもので実際に利用してみない事には本当に良い業者にはめぐり合えないように思います。ネットの情報に惑わされずまずは気になる業者を利用してみて下さい。その中で自分にあった業者を見つけ出すことが最良の方法かと思います。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>宅配買取の点数に制限はありますか？</dt>
        <dd class="faq_list_answer"><i>A</i>特にありませんが点数が少ない場合、ブランドやアイテムによってはお買取をお断りさせていただく場合がございます。上限はございません。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>買取の対象年齢は何歳からですか？</dt>
        <dd class="faq_list_answer"><i>A</i>20歳以上からとなります。未成年の方は保護者もしくは成人の方の名義でのお買取であれば可能です。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>買取の際に必要なものはありますか？</dt>
        <dd class="faq_list_answer"><i>A</i>身分証明書（免許証・保険証等）のコピーが必要となります。その他に買取成立の際のお振込み先の情報（銀行名・支店名・口座番号・名義）も必要となります。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>本物か偽物か鑑定はしてもらえますか？</dt>
        <dd class="faq_list_answer"><i>A</i>大変申し訳ございませんが、鑑定は行なっておりません。弊社での取り扱いのないブランドや買取規定によりお買取できない場合がございますので予めご了承下さいませ。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>事前に連絡をせずに商品を送っても良いでしょうか？</dt>
        <dd class="faq_list_answer"><i>A</i>事前連絡がない場合、申込みの受付はできません。必ず、「申込フォーム」よりお申込みをお願いいたします。※ご連絡がなくお品物を送ってこられた場合、お客様負担で返送させて頂く場合がございます。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>夏に秋冬物、冬に春夏物の買取はしてもらえますか？</dt>
        <dd class="faq_list_answer"><i>A</i>弊社ではオールシーズン対応させて頂いております。詳しくは一度お問い合わせ下さい。</dd>
        <!-- item -->
        <dt class="faq_list_question"><i>Q</i>買い取れない商品はありますか？</dt>
        <dd class="faq_list_answer"><i>A</i>まずは「買取規定」にてご確認ください。買取規定に該当するかご不明な場合は、お客様の商品と商品の状態についてご相談頂ければ、Kindal在籍中の商品補修専門スタッフがおりますので、そちらに確認いたします。</dt>
      </dl>
    </div>
    <div class="section section--brand">
      <?php require_once (dirname(__FILE__) . '/assets/include/brand_list.php'); ?>
    </div>

    <div class="kaitori_banner">
      <?php require_once (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
    </div>
	</div><!-- [end] .page_body -->
</div><!-- [end] .main--faq -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>