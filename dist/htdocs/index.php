<?php
define ('PAGE_CAT' , 'index');
define ('PAGE_ID' , 'home');
define ('PAGE_DESC' , '大阪・心斎橋のブランド古着、アメカジ、アウトドアの高価買取・親切丁寧な得々買い取り・販売情報カインドオル東心斎橋店。使っていないブランド古着・バッグ・靴などございましたら無料、宅配買取査定をご利用ください。 段ボールや伝票をご用意。ご自宅で不要なアイテムを箱に入れるだけ！');
define ('PAGE_TITLE' , '大阪・心斎橋のアメカジ・アウトドア高価買取＆販売＜カインドオル＞');
?>

<?php
include_once (dirname(__FILE__) . '/assets/include/header.php');
?>
	
<div class="main main--<?php echo PAGE_ID; ?>">
	<div class="section section--mv">
		<div class="mv_inner">
			<h1 class="mv_title"><img src="./assets/images/home/mv_title.png" alt="アメカジ＆アウトドア買取"></h1>
			<ul class="mv_kaitori">
				<li class="mv_kaitori_item mv_kaitori_item--shop"><a href="kaitori_shop.php"><img src="./assets/images/home/mv_kaitori01.png" alt="店頭買取"></a></li>
				<li class="mv_kaitori_item mv_kaitori_item--delivery"><a href="kaitori_delivery.php"><img src="./assets/images/home/mv_kaitori02.png" alt="宅配買取"></a></li>
				<li class="mv_kaitori_item mv_kaitori_item--line"><a href="kaitori_line.php"><img src="./assets/images/home/mv_kaitori03.png" alt="LINE査定"></a></li>
			</ul>
		</div>		
		<div class="mv_point">			
			<ul class="mv_point_list">
				<li class="mv_point_list_item"><img src="./assets/images/home/mv_point01.png" alt="親切丁寧な査定をします！"></li>
				<li class="mv_point_list_item"><img src="./assets/images/home/mv_point02.png" alt="状態が悪くても、納得の査定額"></li>
				<li class="mv_point_list_item"><img src="./assets/images/home/mv_point03.png" alt="1万円以上で全品20%UP↑"></li>
				<li class="mv_point_list_item"><img src="./assets/images/home/mv_point04.png" alt="全国「送料・査定無料」で買取"></li>
			</ul>
		</div>
	</div><!-- [end] .section--mv -->
	
	<div class="page_body">
		<div class="section section--kaitori">
			<div class="section_head">
				<h2 class="section_head_title">Kindalの買取ポイント</h2>
			</div>
			<div class="kaitori_point">
				<ul class="point_list">
					<li class="point_list_item point_list_item--point01">
						<h3 class="point_list_item_title">
							<span class="point_list_item_title_image"><img src="./assets/images/point_01.png" alt="01"></span>
							<span class="point_list_item_title_text">親切丁寧な査定を<br>いたします</span>						
						</h3>
						<p class="point_list_item_lead">ほとんどの業者はイメージやバイヤー個人的価値観で調べることをせずに時間短縮で査定をするので概ね相場よりかなり低い査定額になりがちです。<br>当店は１点１点丁寧に調べ中古相場に基づいて査定をします。</p>
					</li>
					<li class="point_list_item point_list_item--point02">
						<h3 class="point_list_item_title">
							<span class="point_list_item_title_image"><img src="./assets/images/point_02.png" alt="02"></span>
							<span class="point_list_item_title_text">状態が悪くても<br>できる限り金額を付けます</span>		
						</h3>
						<p class="point_list_item_lead">一般的な買取業者はメンテナンス（洗濯など）しないので状態が悪いものは極端に低い査定額になります。<br>当店は積極的にメンテナンスを行いますので買取強化しているブランドであれば状態が悪くてもできる限り頑張った査定額にします。</p>
					</li>
				</ul>
				<ul class="point_list">
					<li class="point_list_item point_list_item--point03">
						<h3 class="point_list_item_title">
							<span class="point_list_item_title_image"><img src="./assets/images/point_03.png" alt="03"></span>
							<span class="point_list_item_title_text">査定額の合計が１万円以上<br>になれば全品20％UP</span>								
						</h3>
						<p class="point_list_item_lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</li>
					<li class="point_list_item point_list_item--point04">
						<h3 class="point_list_item_title">
							<span class="point_list_item_title_image"><img src="./assets/images/point_04.png" alt="04"></span>
							<span class="point_list_item_title_text">全国どこでも「送料・査定無料」<br>の宅配買取サービス<br><i class="">（離島などの一部地域除く）</i></span>
						</h3>
						<p class="point_list_item_lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</li>
				</ul>
			</div>
			<div class="kaitori_banner">
				<?php require_once (dirname(__FILE__) . '/assets/include/kaitori_banner.php'); ?>
			</div>
		</div><!-- [end] .section--kaitori -->	

		<div class="section section--result">
			<div class="section_head">
				<h2 class="section_head_title">高価買取実績</h2>
			</div>
			<ul class="post_list">
				<li class="post_list_item">
          <a href="#">
            <div class="post_list_item_image"><img src="./assets/images/bnr_result_montblanc.jpg" alt="MONTBLANC／モンブラン"></div>
            <p class="post_list_item_text">MONTBLANC／モンブラン</p>
          </a>
				</li>
				<li class="post_list_item">
          <a href="#">
					  <div class="post_list_item_image"><img src="./assets/images/bnr_result_berluti.jpg" alt="Berluti／ベルルッティ"></div>
					  <p class="post_list_item_text">Berluti／ベルルッティ</p>
          </a>
				</li>
				<li class="post_list_item">
          <a href="#">
					  <div class="post_list_item_image"><img src="./assets/images/bnr_result_the_real_mccoys.jpg" alt="ザ・リアルマッコイズ"></div>
					  <p class="post_list_item_text">ザ・リアルマッコイズ</p>
          </a>
				</li>
			</ul>
      <div class="button_wrap"><a href="#" class="button--more">高価買取実績をもっと見る</a></div>	
		</div><!-- [end] .section--result -->

		<div class="section section--genre">
			<div class="section_head">
				<h2 class="section_head_title">買取対象商品</h2>
			</div>
			<ul class="genre_list">
				<li class="genre_list_item genre--outdoor"><img src="./assets/images/home/bnr_genre_outdoor.jpg" alt="アウトドア　OutDoor"></p></li>
				<li class="genre_list_item genre--bag"><img src="./assets/images/home/bnr_genre_bag.jpg" alt="バッグ　Bag"></p></li>
				<li class="genre_list_item genre--shoes"><img src="./assets/images/home/bnr_genre_shoes.jpg" alt="靴　Shoes"></p></li>
				<li class="genre_list_item genre--watch"><img src="./assets/images/home/bnr_genre_watch.jpg" alt="時計　Watch"></p></li>
				<li class="genre_list_item genre--glasses"><img src="./assets/images/home/bnr_genre_glasses.jpg" alt="メガネ　Glasses"></p></li>
				<li class="genre_list_item genre--clothing"><img src="./assets/images/home/bnr_genre_clothing.jpg" alt="衣類　Clothing"></p></li>
			</ul>
		</div><!-- [end] .section--genre -->

		<div class="section section--news">
			<div class="section_head">
				<h2 class="section_head_title">ニュース</h2>
			</div>
			<ul class="news_list">
				<li class="news_list_item">
					<time class="news_list_item_date">2019.09.01</time>
					<a href="#" class="news_list_item_lead">セール｜いつでも10％OFFサービス</a>
				</li>
				<li class="news_list_item">
					<time class="news_list_item_date">2019.09.01</time>
					<a href="#" class="news_list_item_lead">【心斎橋】お勧めのデートスポットカップルで行きたい場所Ⅲ </a>
				</li>
				<li class="news_list_item">
					<time class="news_list_item_date">2019.09.01</time>
					<a href="#" class="news_list_item_lead">長い文章長い文章長い文章長い文章長い文章長い文章長い文章長い文章長い文章長い文章長い文章長い文章</a>
				</li>
			</ul>
      <div class="button_wrap"><a href="#" class="button--more">ニュースをもっと見る</a></div>
		</div><!-- [end] .section--news -->

		<div class="section section--latest">
			<div class="latest_column">
				<div class="section_head">
					<h2 class="section_head_title">コラム</h2>
				</div>
				<div class="section_body">
					<ul class="latest_list">
						<li class="latest_list_item">
							<div class="latest_list_thumb"><img src="./assets/images/dummy_thumb01.jpg" alt=""></div>
							<div class="latest_list_text">
								<p class="latest_list_text_date">2019.09.01</p>
								<p class="latest_list_text_lead">ダウンジャケットの定番ブランドカナダグースの人気アイテムといえばジャスパー/JASPER！</p>
							</div>	
						</li>
						<li class="latest_list_item">
							<div class="latest_list_thumb"><img src="./assets/images/dummy_thumb02.jpg" alt=""></div>
							<div class="latest_list_text">
								<p class="latest_list_text_date">2019.09.01</p>
								<p class="latest_list_text_lead">ダウンジャケットの定番ブランドカナダグースの人気アイテムといえばジャスパー/JASPER！</p>
							</div>	
						</li>
						<li class="latest_list_item">
							<div class="latest_list_thumb"><img src="./assets/images/dummy_thumb03.jpg" alt=""></div>
							<div class="latest_list_text">
								<p class="latest_list_text_date">2019.09.01</p>
								<p class="latest_list_text_lead">ダウンジャケットの定番ブランドカナダグースの人気アイテムといえばジャスパー/JASPER！</p>
							</div>	
						</li>
					</ul>
					<div class="button_wrap"><a href="#" class="button--more">コラムをもっと見る</a></div>
				</div>
			</div><!-- [end] .latest_column -->

			<div class="latest_blog">
				<div class="section_head">
					<h2 class="section_head_title">オーナーブログ</h2>
				</div>
				<div class="section_body">
					<ul class="latest_list">
						<li class="latest_list_item">
							<div class="latest_list_thumb"><img src="./assets/images/dummy_thumb04.jpg" alt=""></div>
							<div class="latest_list_text">
								<p class="latest_list_text_date">2019.09.01</p>
								<p class="latest_list_text_lead">ダウンジャケットの定番ブランドカナダグースの人気アイテムといえばジャスパー/JASPER！</p>
							</div>	
						</li>
						<li class="latest_list_item">
							<div class="latest_list_thumb"><img src="./assets/images/dummy_thumb05.jpg" alt=""></div>
							<div class="latest_list_text">
								<p class="latest_list_text_date">2019.09.01</p>
								<p class="latest_list_text_lead">ダウンジャケットの定番ブランドカナダグースの人気アイテムといえばジャスパー/JASPER！</p>
							</div>	
						</li>
						<li class="latest_list_item">
							<div class="latest_list_thumb"><img src="./assets/images/dummy_thumb06.jpg" alt=""></div>
							<div class="latest_list_text">
								<p class="latest_list_text_date">2019.09.01</p>
								<p class="latest_list_text_lead">ダウンジャケットの定番ブランドカナダグースの人気アイテムといえばジャスパー/JASPER！</p>
							</div>	
						</li>
					</ul>
					<div class="button_wrap"><a href="#" class="button--more">ブログをもっと見る</a></div>
				</div>
			</div><!-- [end] .latest_blog -->
		</div><!-- [end] .section--latest -->
	</div><!-- [end] .page_body -->
</div><!-- [end] .main -->

<?php
require_once (dirname(__FILE__) . '/assets/include/footer.php');
?>